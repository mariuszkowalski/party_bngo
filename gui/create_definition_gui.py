#!/usr/bin/env python3


from tkinter import GROOVE, SUNKEN, StringVar
from tkinter import ttk

from gui.main_gui import MainGui


class DefinitionEntryGroup(MainGui):
    
    def __init__(self, parent_frame, definitions, settings, number=1, *args, **kwargs):
        super().__init__(parent_frame, settings, *args, **kwargs)

        self.number = number
        self.definitions = definitions
        self.entry_value = StringVar()
        temp = 'Placecholder {}...'.format(self.number+1)

        self.entry_value.set(temp)

        self.group_frame = ttk.LabelFrame(
            self.main_frame,
            width=self.settings.definition_entry_group_width,
            height=self.settings.definition_entry_group_height,
            relief=GROOVE,
            text='Field {}. Type to change. Press "Enter" to confirm'.format(self.number+1)
            )

        # Variables for creation of the grid layout.
        _x = 14 + (self.settings.definition_entry_group_width + 5) * (self.number // self.settings.number_of_rows_for_definitions)
        _y = 40 + (self.settings.definition_entry_group_height + 3) * (self.number % self.settings.number_of_rows_for_definitions)
        
        self.group_frame.place(x=_x, y=_y)

        self.definition_entry = ttk.Entry(self.group_frame, width=65, textvariable=self.entry_value)
        self.definition_entry.place(x=4, y=2)
        # self.definition_entry.bind('<Return>', lambda event, number=self.number:self.update_definition(event, number))
        self.definition_entry.bind('<Return>', self.update_definition)
        self.definition_entry.bind('<Tab>', self.update_definition)
        self.definition_entry.bind('<Leave>', self.update_definition)
        
        self.gui_elements = [
            self.group_frame,
            self.definition_entry
            ]

    def update_definition(self, event):
        """
        Updates definition list, gets text value from the entry.
        """

        self.definitions.definitions[self.number] = self.definition_entry.get()


class CrateDefinitionGui(MainGui):
    """
    CreateDefinitionGui main class, containing parent elements.
    """

    def __init__(self, parent_frame, definitions, settings, *args, **kwargs):
        super().__init__(parent_frame, settings, *args, **kwargs)
        """
        Initializes class, inherits from MainGui.

        Args:
            parent frame: instance of ttk.Frame - parent object of given group of elements.
            definitions: instance of Definitions - contains list with the defined texts to draft from.
            settings: instance of Settings - contains all program settings.
        """

        self.settings = settings
        
        self.definitions = definitions[0]
        self.definitions.add_placeholders()

        self.definition_groups = []

        # Resets player menu to aviable state.
        self.settings.player_menu_avaliable = True
        
        self.info_text_value = 'Fill 25 fields to achive best results. If more filled, values are going to appear randomly in the board.'
        self.info_text = ttk.Label(self.main_frame, font=('Helvetica', 12), text=self.info_text_value)
        self.info_text.place(x=15, y=5)

        self.apply = ttk.Button(self.main_frame, text='Apply')
        self.apply.place(x=self.settings.window_width-308, y=7)
        self.apply.bind('<Button-1>', self.apply_definition)

        for i in range(36):
            self.add_definition_group(i)
        
        # self.definition_groups[0].definition_entry.focus()
        self.definition_groups[0][0].definition_entry.focus()

        self.gui_elements = [
            self.info_text,
            self.apply
            ]

    def clear(self):
        """
        Override of the clear method inhareted from MainGui.
        """

        for group in self.definition_groups:
            for element in group:
                element.clear()

        for element in self.gui_elements:
            element.destroy()

    def add_definition_group(self, i):
        """
        Add single definition group to the definition groups list.
        """

        deg = [
            DefinitionEntryGroup(
                self.main_frame,
                self.definitions,
                self.settings,
                number=i
                )
            ]
        self.definition_groups.append(deg)

    def apply_definition(self, event):
        """
        Applies current definition and creates new game for the current number of players.
        """
        if self.settings.debug:
            print('apply definition.')
        
        self.clear()
        # Turns off controlls
        self.settings.definitions_gui_exists = False
        # Locks the player menu.
        self.settings.player_menu_avaliable = False
