#!/usr/bin/env python3


from tkinter import ttk


class MainGui:
    """
    Basic gui class, contains all necessary elements.
    """

    def __init__(self, parent_frame, settings, *args, **kwargs):
        """
        Creats basic instance of Tkinter gui element.

        Args:
            parent frame: instance of ttk.Frame - parent object of given group of elements.
            settings: instance of Settinfs - contains all program settings.
        """

        self.main_frame = parent_frame
        self.settings = settings

        self.gui_elements = []

    def clear(self):
        """
        Destroys all elements in the the gui_elements list.
        For easy removal of unwanteg groups of elements.
        """

        for element in self.gui_elements:
            element.destroy()

    def add(self, element):
        """
        Adds elements to the gui_elements list.
        Use only for top level parent object.
        """

        self.gui_elements.append(element)
        