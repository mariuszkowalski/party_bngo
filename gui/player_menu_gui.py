#!/usr/bin/env python3


from tkinter import GROOVE, StringVar
from tkinter import ttk

from core.player import Player
from core.players import Players
from gui.main_gui import MainGui


class PlayerEntryGroup(MainGui):
    """
    Contains player group GUI elements Frame and Entry.
    Manages adding player and player removal.
    """
    
    total = 0

    def __init__(self, parent_frame, settings, *args, **kwargs):
        super().__init__(parent_frame, settings, *args, **kwargs)
        """
        Creates group of GUI elements.

        static:
            total: int - number of active players, number of instnces of the cls.
                Used only for position of given group.

        Args:
            parent frame: instance of ttk.Frame - parent object of given group of elements.
            settings: instance of Settings - contains all program settings.
            current_players: instance of Players - contains all players assigned to the game.
            total: static int of PlayerEntryGroup - contains info about number of instances
                the cls.
        """

        self.current_players = kwargs.get('current_players')
        PlayerEntryGroup.total = kwargs.get('total')
        self.auto_created_name = 'Player {}'.format(len(self.current_players)+1)
        self.auto_created_number = len(self.current_players)

        self.current_player_name = StringVar()
        self.current_player_name.set(self.auto_created_name)
        
        #Add player to players.
        player = Player(self.auto_created_name)
        self.current_players.add(player)

        self.player_frame = ttk.Frame(self.main_frame, width=195, height=40, relief=GROOVE)
        self.player_frame.place(x=5, y=40+(PlayerEntryGroup.total*42))
        
        self.player_entry = ttk.Entry(self.player_frame, width=30, textvariable=self.current_player_name)
        self.player_entry.bind(
            '<Leave>', lambda event, cpl=self.current_player_name, cpn=self.auto_created_number: self.update_player(event, cpl, cpn)
            )
        self.player_entry.bind(
            '<Return>', lambda event, cpl=self.current_player_name, cpn=self.auto_created_number: self.update_player(event, cpl, cpn)
            )
        self.player_entry.place(x=5, y=5)

        self.gui_elements = [self.player_frame, self.player_entry]

    def update_player(self, event, current_player_name, current_player_number):
        """
        Updates given player's name.

        Args:
            event: x y pos - mouse position and event.
            current_player_name: str - player;s attached to given entry field.
            current_player_number: int - number assigned to the player, used for defult name convention.
        """

        if self.settings.debug:
            print('Debug current_player_name: {}, current_player_number: {}.'.format(current_player_name.get(), current_player_number))
            print('Debug player menu avaliable: {}.'.format(self.settings.player_menu_avaliable))
        
        old_name = self.current_players.players[current_player_number].name
        
        # Checks if the player menu was not disebled by the game start.
        if self.settings.player_menu_avaliable:

            # Checks it the name is unique.
            if not self.current_players.check(current_player_name.get()):
                self.current_players.players[current_player_number].name = current_player_name.get()
            else:
                self.current_player_name.set(old_name)

        if self.settings.debug:
            print('New name: "{}"'.format(self.current_players.players[current_player_number].name))


class PlayerMenuGui(MainGui):
    """
    Main part of the player menu gui.
    """

    def __init__(self, parent_frame, current_players, settings, *args, **kwargs):
        super().__init__(parent_frame, settings, *args, **kwargs)
        """
        Creates player menu.

        Args:
            parent frame: instance of ttk.Frame - parent object of given group of elements.
            settings: instance of Settings - contains all program settings.
            current_players: instance of Players - contains all players assigned to the game.
        """
        
        #instance of Players
        self.current_players = current_players[0]
        self.current_player_fields = []

        self.main_frame.bind('<Enter>', self.current_player_fields_togle)

        self.add_player_button = ttk.Button(
            self.main_frame,
            width=14,
            text='Add player'
            )
        self.add_player_button.bind('<Button-1>', self.add_player)
        self.add_player_button.place(x=5, y=5)

        self.remove_player_button = ttk.Button(
            self.main_frame,
            width=14,
            text='Remove player'
            )
        self.remove_player_button.bind('<Button-1>', self.remove_player)
        self.remove_player_button.place(x=105, y=5)

        # Contain instances of PlayerEntryGroup
        self.player_fields = []
        
    def add_player(self, event):
        """
        Add new player to the game.

        Args:
            event: x y pos - mouse position and event.
        """

        if len(self.player_fields) < 10 and self.settings.player_menu_avaliable:
            
            peg = [
                PlayerEntryGroup(
                    self.main_frame,
                    self.settings,
                    total=len(self.player_fields),
                    current_players=self.current_players
                    )
                ]
            
            if self.settings.debug:
                print('Debug number of players: {}'.format(len(self.current_players)))

            self.player_fields.append(peg)
        
        else:
            print('Can not add more players.')

    def remove_player(self, event):
        """
        Remove last player from the game.

        Args:
            event: x y pos - mouse position and event.
        """

        if len(self.player_fields) > 0 and self.settings.player_menu_avaliable:
            
            for element in self.player_fields[-1]:
                # This goes inside list of all fields.
                for gui_element in element.gui_elements:
                    # This goes through every element in the list: frame, entry.
                    gui_element.destroy()
            
            del self.player_fields[-1]
            del self.current_players.players[-1]
            
            if self.settings.debug:
                print('Debug number of players: {}'.format(len(self.current_players)))

        else:
            print('No players to remove.')

    def current_player_fields_togle(self, event):
        """
        Turn on and off entriers in player menu.
        Depends on the player_menu_aviable: bool setting.
        """

        # Removal of binding not required.
        if not self.settings.player_menu_avaliable:
            for field in self.player_fields:
                for element in field:
                    element.player_entry.config(state='disabled')
        else:
            for field in self.player_fields:
                for element in field:
                    element.player_entry.config(state='enabled')
            
