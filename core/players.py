#!/usr/bin/env python3


from core.player import Player


class Players:
    """
    Contains list of the current players in game.
    """

    def __init__(self):
        """
        Crates list of the players initialized at the start of the program.
        """
        
        self.players = []

    def __len__(self):
        """
        Returns numbers of the active players.
        """
        
        return len(self.players)

    def __repr__(self):
        """
        Returns printable representation of object.
        """

        names = []
        for element in self.players:
            names.append(element.name)

        return '; '.join(names)

    def add(self, player):
        """
        Adds new player to the pool, instance of the Player

        Args:
            player: instance of Player - new added player.
        """
        if isinstance(player, Player):
            pass
        else:
            raise TypeError('player not of class Player.')
        
        for p in self.players:
            if player.name == p.name:
                raise ValueError('player name not unique.')
        
        self.players.append(player)

    def remove(self, player_name):
        """
        Remove player with the given name.

        Args:
            player_name: str - player name to beeing removed.
        """

        for i, player in enumerate(self.players):
            if player.name == player_name:
                self.players = self.players[:i] + self.players[i+1:]

    def check(self, player_name):
        """
        Check if given player name exists amongs players.

        Args:
            player_name: str - player name to beeing removed.
        """

        for player in self.players:
            if player.name.upper() == player_name.upper():
                return True
        else:
            return False
