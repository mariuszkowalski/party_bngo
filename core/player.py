#!/usr/bin/env python3


class Player:
    """
    Game player instance
    """

    def __init__(self, name):
        """
        Creates player instance for the game.
        
        Args:
            name: str - name of the player.
            board: list - placement of the game fields.
        """

        self.name = name
        self.board = []
