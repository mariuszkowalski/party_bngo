#!/usr/bin/env python3


from settings.settings import WindowSettings


class Definitions:

    def __init__(self, nod=36):
        self.nod = nod
        self.index = 0
        self.definitions = ['' for x in range(self.nod)]

    def __len__(self):
        return len(self.definitions)

    def __repr__(self):
        return '; '.join(self.definitions)

    def __iter__(self):
        return self

    def __next__(self):
        try:
            result = self.definitions[self.index]

        except IndexError:
            self.index = 0
            raise StopIteration
        
        self.index += 1
        return result

    def add_placeholders(self):
        self.definitions = ['Placecholder {}...'.format(x+1) for x in range(self.nod)]

    def load(self, alist):
        self.definitions = alist

