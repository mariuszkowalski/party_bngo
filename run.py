#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from tkinter import  Tk, Menu, W, SUNKEN, GROOVE, StringVar
from tkinter import ttk
from tkinter import messagebox
from tkinter.filedialog import askopenfilename, asksaveasfilename
import codecs
import os
import random
import sys

from core.game import Game
from core.players import Players
from core.definitons import Definitions

from gui.create_definition_gui import CrateDefinitionGui
from gui.player_menu_gui import PlayerMenuGui
from settings.settings import WindowSettings


DIRECTORY = os.path.dirname(os.path.abspath(__file__)).replace('\\', '/')
DEFNITIONS_DIRECTORY = os.path.join(DIRECTORY, 'saved_definitions').replace('\\', '/')


class Window:
    
    def __init__(self, main_widget, settings):

        self.main_widget = main_widget
        self.settings = settings

        self.current_game = [Game()]
        self.current_players = [Players()]
        self.definitions = [Definitions(nod=self.settings.number_of_definitions)]
        
        # Existance value for menu elements.

        # Base lvl of gui.
        self.status_bar_text = StringVar()

        self.drop_down_menu = Menu(self.main_widget)
        self.main_widget.config(menu=self.drop_down_menu)

        self.file_menu = Menu(self.drop_down_menu, tearoff=0)
        self.drop_down_menu.add_cascade(label='File', menu=self.file_menu)

        self.file_menu.add_command(label='Create Definition', command=self.create_definition, accelerator='Ctrl+N')
        self.file_menu.add_command(label='Save Definition', command=self.save_definition, accelerator='Ctrl+S')
        self.file_menu.add_command(label='Open Definition', command=self.open_definition, accelerator='Ctrl+O')
        
        self.file_menu.add_separator()
        self.file_menu.add_command(label='Quit', command=self.main_widget.quit, accelerator='Ctrl+Q')
        
        self.game_menu = Menu(self.drop_down_menu, tearoff=0)
        self.drop_down_menu.add_cascade(label='Game', menu=self.game_menu)

        #self.game_menu.add_command(label='New Game', command=self.new_game, accelerator='Ctrl+U')
        #self.file_menu.add_separator()
        self.game_menu.add_command(label='Save Game', command=self.save_game, accelerator='Ctrl+J')
        self.game_menu.add_command(label='Load Game', command=self.load_game, accelerator='Ctrl+K')

        if self.settings.debug:
            self.debug_menu = Menu(self.drop_down_menu, tearoff=0)
            self.drop_down_menu.add_cascade(label='Debug', menu=self.debug_menu)

            self.debug_menu.add_command(label='Debug players', command=self.debug_players)

        
        self.main_frame = ttk.Frame(
            self.main_widget,
            width=self.settings.window_width,
            height=self.settings.window_height-20,
            padding=(0, 0, 0, 0)
            )
        self.main_frame.place(x=0, y=0)
        self.main_frame.bind_all('<Control-n>', self.create_definition_handler)
        self.main_frame.bind_all('<Control-s>', self.save_definition_handler)
        self.main_frame.bind_all('<Control-o>', self.open_definition_handler)
        self.main_frame.bind_all('<Control-q>', self.quit_app_handler)
        self.main_frame.bind_all('<Control-u>', self.new_game_handler)
        self.main_frame.bind_all('<Control-j>', self.save_game_handler)
        self.main_frame.bind_all('<Control-k>', self.load_game_handler)
        
        self.status_bar = ttk.Label(
            self.main_widget,
            width=self.settings.window_width,
            anchor=W,
            border=0,
            relief=SUNKEN,
            textvariable=self.status_bar_text
            )
        self.status_bar.place(x=0, y=self.settings.window_height-19)

        self.players_frame = ttk.LabelFrame(
            self.main_frame,
            width=210,
            height=self.settings.window_height-22,
            text='Players')
        self.players_frame.place(x=3, y=0)

        self.side_frame = ttk.Frame(
            self.main_frame,
            width=self.settings.window_width-219,
            height=self.settings.window_height-30,
            relief=GROOVE
            )
        self.side_frame.place(x=215, y=8)

        self.player_menu = [
            PlayerMenuGui(
                self.players_frame,
                self.current_players,
                self.settings
                )
            ]
        
    def create_definition_handler(self, event):
        """
        Create definition handler, for the mouse events in the menu.
        """

        self.create_definition()

    def create_definition(self):
        if not self.settings.definitions_gui_exists:
            print('Create definition.')
        
            self.create_definition_gui = [
                CrateDefinitionGui(
                    self.side_frame,
                    self.definitions,
                    self.settings
                    )
                ]
            self.settings.definitions_gui_exists = True
        
        else:
            if self.settings.debug:
                print('Definitions gui already exists.')
            
            # Destroy old elements.
            self.create_definition_gui[0].clear()
            del self.create_definition_gui

            # Create new again.
            self.create_definition_gui = [
                CrateDefinitionGui(
                    self.side_frame,
                    self.definitions,
                    self.settings
                    )
                ]
            
    def save_definition_handler(self, event):
        """
        Save definition handler, for the mouse events in the menu.
        """

        print('Save definition.')
        self.save_definition()

    def save_definition(self):
        """
        Saves the definition to the file.
        Default path: directory/save_definitions
        Default extension: .pdb
        Encoding: utf-8
        """

        if self.settings.debug:
            print('Save definitions clicked.')
            
        definitions_present = False
        raw_file_name_to_save = ''

        allowed_file_types = ['.pbd', '.PDB']

        try:
            if self.definitions[0] != [Definitions(nod=self.settings.number_of_definitions)]:
                definitions_present = True
            else:
                raise ValueError
        
        except ValueError:
            messagebox.showinfo(message='No definitions to save!')

        if definitions_present:
            raw_file_name_to_save = asksaveasfilename(
                defaultextension=".pdb",
                initialdir=DEFNITIONS_DIRECTORY,
                filetypes=(('PB Definitions', '*.pdb *.PDB'), ('All files', '*.*'))
                )

        file_name_esists = True if len(raw_file_name_to_save) > 0 else False

        if file_name_esists and definitions_present:
            
            try:
                # raw_file_name_to_save += '.pdb'
                path_to_write = os.path.join(DIRECTORY, raw_file_name_to_save).replace('\\', '/')
                file_to_write = codecs.open(path_to_write, 'w', 'utf-8')

            except OSError:
                messagebox.showinfo(message='File name not selected!')
                
            else:
                for line in self.definitions[0]:
                    temp = '{}\n'.format(line)
                    file_to_write.write(temp)
                file_to_write.close()

    def open_definition_handler(self, event):
        """
        Open definition handler, for the mouse events in the menu.
        """
        
        print('Open definition.')
        self.open_definition()

    def open_definition(self):
        """
        Opens the definition from the file.
        Default path: directory/save_definitions
        Default extension: .pdb
        Encoding: utf-8
        """

        if self.settings.debug:
            print('Open definition clicked!')

        allowed_file_types = ['.pbd', '.PDB']
        self.file_name = askopenfilename(
            initialdir = DEFNITIONS_DIRECTORY,
            filetypes=(('PB Definitions', '*.pdb *.PDB'), ('All files', '*.*'))
            )

        print(self.file_name, self.file_name[-4:])

        try:
            if len(self.file_name) == 0:
                raise OSError
            elif len(self.file_name) > 0 and self.file_name[-4:] in allowed_file_types:
                raise AttributeError
            else:
                file_content_raw = [x.strip() for x in codecs.open(self.file_name, 'r', 'utf-8').readlines() if len(x) > 0]

        except OSError:
            messagebox.showinfo(message='File not selected!')

        except AttributeError:
            messagebox.showinfo(message='Not supported file type!')

        else:
            # File content assesment.
            if 36 < len(file_content_raw) < 25:
                messagebox.showinfo(message='File does not have required format!')
            else: 
                self.definitions[0].load(file_content_raw)
                
                if self.settings.debug:
                    print(self.definitions[0])
         
    def new_game_handler(self, event):
        """
        New game handler, for the mouse events in the menu.
        """
        
        print('New game.')
        self.new_game()

    def new_game(self):
        pass

    def save_game_handler(self, event):
        """
        Save game handler, for the mouse events in the menu.
        """

        print('Save game.')
        self.save_game()

    def save_game(self):
        pass

    def load_game_handler(self, event):
        """
        Load game handler, for the mouse events in the menu.
        """
                
        print('Load game.')
        self.load_game()

    def load_game(self):
        pass

    def quit_app_handler(self, event):
        self.main_widget.destroy()

    def debug_players(self):
        print(self.current_players)


def main():
    offset = -1800 if sys.argv[1] == 'o' else 100
    
    window_settings = WindowSettings()
    root = Tk()
    root.geometry('{}x{}+{}+100'.format(window_settings.window_width, window_settings.window_height, offset))
    root.title('Party BNGO')
    
    window = Window(root, window_settings)

    root.mainloop()


if __name__ == '__main__':
    main()