#!/usr/bin/env python3


import unittest

from core.player import Player
from core.players import Players


class PlayersCase1(unittest.TestCase):
    
    def test_length_1(self):
        players = Players()
        self.assertEqual(0, len(players), 'Should be 0.')
    
    def test_instance_check(self):
        players = Players()
        player = Player('aaaa')
        players.add(player)
        self.assertIsInstance(player, Player, 'Should be instance of Player.')

    def test_length_2(self):    
        players = Players()
        player = Player('aaaa')
        players.add(player)
        self.assertEqual(1, len(players), 'Should be 1.')
    
    def test_name_unique(self):    
        players = Players()
        player = Player('aaaa')
        players.add(player)
        player = Player('aaaa')
        self.assertRaises(ValueError, players.add, player)

    def test_check_name(self):
        players = Players()
        player = Player('aaaa')
        players.add(player)
        self.assertEqual(True, players.check('aaaa'))

    def test_remove_player(self):
        players = Players()
        player1 = Player('aaa')
        player2 = Player('bbb')
        player3 = Player('ccc')
        players.add(player1)
        players.add(player2)
        players.add(player3)
        players.remove('bbb')
        self.assertEqual(2, len(players), 'Should be 2.')


def main():
    unittest.main()


if __name__ == '__main__':
    main()