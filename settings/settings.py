#!/usr/bin/env python3


class WindowSettings:
    
    def __init__(self):


        self.window_width = 1500
        self.window_height = 700
        self.debug = True

        self.number_of_definitions = 36
        self.number_of_rows_for_definitions = 12
        self.definition_entry_group_width = 415
        self.definition_entry_group_height = 48

        self.definitions_gui_exists = False
        self.game_exists = False

        self.player_menu_avaliable = True